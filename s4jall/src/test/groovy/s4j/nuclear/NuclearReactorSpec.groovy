package s4j.nuclear;

import spock.lang.*;


public class NuclearReactorMonitor {
    private int pressureInBar;
    private int sensorsDetectingFire;
    private List<Long> radiationStatus;
    private boolean radiationPressent;

    public void feedData(int pressureInBar, int triggeredFireSensors, List<Float> radiationStatus) {
        feedPressureInBar(pressureInBar);
        feedFireSensorData(triggeredFireSensors);
        feedRadiationSensorData(radiationStatus);
    }

    public void feedData(Map data) {
        feedData(data["pressureInBar"], data["triggeredFireSensors"], data["radiationStatus"]);
    }

    public void feedPressureInBar(int bar) {
        pressureInBar = bar;
    }

    public void feedFireSensorData(int triggeredFireSensors) {
        sensorsDetectingFire = triggeredFireSensors;
    }

    public void feedRadiationSensorData(List<Float> radiationStatus) {
        this.radiationStatus = radiationStatus;
        this.radiationPressent = evalRadiationPressent();
    }

    public WarningStatus getCurrentStatus() {
        boolean alarmStatus = (pressureInBar > 150 || sensorsDetectingFire > 0 || radiationPressent);
        boolean shutDownNeeded = (pressureInBar > 160 || sensorsDetectingFire > 2 || radiationPressent);
        int evacuationMinutes = evalEvacuationTime();

        return new WarningStatus(alarmStatus, shutDownNeeded, evacuationMinutes,
            pressureInBar, sensorsDetectingFire, radiationStatus);
    }

    private boolean evalRadiationPressent() {
        for (Float radiationMeter:radiationStatus) {
            if(radiationMeter > 100) {
                return true;
            }
        }
        return false;
    }
    
    private int evalEvacuationTime() {
        if (radiationPressent) {
            return 1;
        } else if (pressureInBar > 160) {
            return 3;
        } else {
            return -1;
        }
    }

}

class WarningStatus {

    private final boolean alarmActive = false;
    private final boolean shutDownNeeded = false;
    private final int evacuationMinutes = -1;
    private final int pressure;
    private final int triggeredFireSensors;
    private final List<Long> radiation;

    public WarningStatus(boolean alarmActive, boolean shutDownNeeded, int evacuationMinutes,
            int pressure, int triggeredFireSensors, List<Long> radiation) {
        this.alarmActive = alarmActive;
        this.shutDownNeeded = shutDownNeeded;
        this.evacuationMinutes = evacuationMinutes;
        this.pressure = pressure;
        this.triggeredFireSensors = triggeredFireSensors;
        this.radiation = radiation;
    }

    public boolean evacuationNeeded() {
        return evacuationMinutes != -1
    }
}

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

//@RunWith(Parameterized.class)
public class NuclearReactorTest {

    //public NuclearReactorTest(int pressure, int triggeredSensors,
    //    List<Float> radiationDataReadings, boolean expectedAlarmStatus,
    //    boolean expectedShutdownCommand, int expectedMinutesToEvacuate) {
    //}

    //@Parameters
    //public static Collection<Object[]> data() {
    //    return Arrays.asList(new Object[][] {
    //            { 1, 1, new ArrayList<Float>(), false, false, -1}
    //        });
    //}

    @Test
    public void nuclearReactorScenario() {
        assertEquals(true, true);
    }
}

class NuclearReactorMonitorSpec extends spock.lang.Specification {

    /**
     * If pressure is less 150, no fire triggered, and not has radiation leak, everithing is ok.
     */
    def "If everithing is ok"() {
        given: "that pressure is 0, all fire sensor are off and all radiation status are 0"
        NuclearReactorMonitor reactorMonitor = new NuclearReactorMonitor();
        int pressure = 0;
        int triggeredFireSensors = 0;
        List<Float> radiationStatus = [0f, 0f, 0f, 0f];

        when: "we ask the status of pressure, fire control, and radiation sensors"
        reactorMonitor.feedPressureInBar(pressure);
        reactorMonitor.feedFireSensorData(triggeredFireSensors);
        reactorMonitor.feedRadiationSensorData(radiationStatus);
        WarningStatus status = reactorMonitor.getCurrentStatus();


        then: "no alarm/notification and evacuation should be triggered"
        !status.alarmActive;
        !status.shutDownNeeded
        !status.evacuationNeeded()
        status.evacuationMinutes == -1
    }


    /**
     * If pressure goes above 150 bars, the alarm sounds.
     */
    def "If pressure goes above 150 bars, the alarm sounds"() {
        given: "that pressure is 160"
        NuclearReactorMonitor reactorMonitor = new NuclearReactorMonitor();
        int pressure = 160;
        int triggeredFireSensors = 0;
        List<Float> radiationStatus = [0f, 0f, 0f, 0f];

        when: "we ask the status of pressure, fire control, and radiation sensors"
        reactorMonitor.feedPressureInBar(pressure);
        reactorMonitor.feedFireSensorData(triggeredFireSensors);
        reactorMonitor.feedRadiationSensorData(radiationStatus);
        WarningStatus status = reactorMonitor.getCurrentStatus();


        then: "only alarm should be triggered"
        status.alarmActive;
        !status.shutDownNeeded;
        !status.evacuationNeeded();
        status.evacuationMinutes == -1;
    }

    /**
     * If one o two fire alarms are triggered, the alarm sounds.
     */
    def "If one o two fire alarms are triggered, the alarm sounds"() {
        given: "that one fire sensors are active"
        NuclearReactorMonitor reactorMonitor = new NuclearReactorMonitor();
        int pressure = 0;
        int triggeredFireSensors = 1;
        List<Float> radiationStatus = [0f, 0f, 0f, 0f];

        when: "we ask the status of pressure, fire control, and radiation sensors"
        reactorMonitor.feedPressureInBar(pressure);
        reactorMonitor.feedFireSensorData(triggeredFireSensors);
        reactorMonitor.feedRadiationSensorData(radiationStatus);
        WarningStatus status = reactorMonitor.getCurrentStatus();


        then: "only alarm should be triggered"
        status.alarmActive;
        !status.shutDownNeeded;
        !status.evacuationNeeded();
        status.evacuationMinutes == -1;
    }


    /**
     * If pressure goes above 160 bars, the alarm sounds,
     * and announcement is made that the reactor should be evacuated withing the next 3 minutes,
     * and a notification is sent to the human operator that a shutdown is needed.
     */
    def "If pressure goes above 160 bars, the alarm sounds, should evacuate in 3 minutes, and shutdown is needed"() {
        given: "that pressure is 165"
        NuclearReactorMonitor reactorMonitor = new NuclearReactorMonitor();
        int pressure = 165;
        int triggeredFireSensors = 0;
        List<Float> radiationStatus = [0f, 0f, 0f, 0f];

        when: "we ask status of pressure, fire control, and radiation sensors"
        reactorMonitor.feedPressureInBar(pressure);
        reactorMonitor.feedFireSensorData(triggeredFireSensors);
        reactorMonitor.feedRadiationSensorData(radiationStatus);
        WarningStatus status = reactorMonitor.getCurrentStatus();

        then: "alarm and shutdown notification are triggered and announcement evacuated withing next 3 mintues"
        status.alarmActive;
        status.shutDownNeeded;
        status.evacuationNeeded();
        status.evacuationMinutes == 3;
    }

    /**
     * If two or more fire alarms are triggered, the alarm sounds
     * and the operator is notified that a shutdown is needed.
     */
    def "If two or more fire alarms are triggered, the alarm sounds, and shutdown is need"() {
        given: "that three fire sensors are active"
        NuclearReactorMonitor reactorMonitor = new NuclearReactorMonitor();
        int pressure = 0;
        int triggeredFireSensors = 3;
        List<Float> radiationStatus = [0f, 0f, 0f, 0f];

        when: "we ask status of pressure, fire sensors, and radiation sensors"
        reactorMonitor.feedPressureInBar(pressure);
        reactorMonitor.feedFireSensorData(triggeredFireSensors);
        reactorMonitor.feedRadiationSensorData(radiationStatus);
        WarningStatus status = reactorMonitor.getCurrentStatus();

        then: "alarm and shutdown notification are triggered"
        status.alarmActive;
        status.shutDownNeeded;
        !status.evacuationNeeded();
        status.evacuationMinutes == -1;
    }

    /**
    * If a radiation leak is detected (100+ rads from any sensor), the alarm sounds,
    * and announcement is made that the reactor should be evacuated withing the next minute,
    * and a notification is sent to the human operator that a shutdown is needed.
    */
    def "If a radiation leak detected, the alarm sounds, should evacuate in 1 minutes, and shutdown is needed"() {
        given: "that one radiation detect 110 rads"
        NuclearReactorMonitor reactorMonitor = new NuclearReactorMonitor();
        int pressure = 0;
        int triggeredFireSensors = 0;
        List<Float> radiationStatus = [110f, 0f, 0f, 0f];

        when: "we ask status of preassure, fire sensors, and radiation sensors"
        reactorMonitor.feedPressureInBar(pressure)
        reactorMonitor.feedFireSensorData(triggeredFireSensors);
        reactorMonitor.feedRadiationSensorData(radiationStatus);
        WarningStatus status = reactorMonitor.getCurrentStatus();

        then: "alarm and shutdown notification are triggered, and announcement evacuated for next minute"
        status.alarmActive;
        status.shutDownNeeded;
        status.evacuationNeeded();
        status.evacuationMinutes == 1;
    }

    def "Complete test of all nuclear scenarios"() {

        given: "a nuclear reactor and sensor data"
        NuclearReactorMonitor reactorMonitor = new NuclearReactorMonitor();

        when: "we examine the sensor data"
        reactorMonitor.feedPressureInBar(pressure)
        reactorMonitor.feedFireSensorData(fireSensors);
        reactorMonitor.feedRadiationSensorData(radiation);
        WarningStatus status = reactorMonitor.getCurrentStatus();

        then: "we act according to safety requirements"
        status.alarmActive == alarm
        status.shutDownNeeded == shutDown
        status.evacuationMinutes == evacuation
        status.pressure == pressure
        status.triggeredFireSensors == fireSensors
        status.radiation == radiation

        where: "possible nuclear incidents are:"
        pressure | fireSensors | radiation             || alarm | shutDown | evacuation
        150      | 0           | []                    || false | false    | -1
        150      | 1           | []                    || true  | false    | -1
        150      | 3           | []                    || true  | true     | -1
        150      | 0           | [110.4f ,0.3f, 0.0f]  || true  | true     | 1
        150      | 0           | [45.3f ,10.3f, 47.7f] || false | false    | -1
        155      | 0           | [0.0f ,0.0f, 0.0f]    || true  | false    | -1
        170      | 0           | [0.0f ,0.0f, 0.0f]    || true  | true     | 3
        180      | 0           | [110.4f ,0.3f, 0.0f]  || true  | true     | 1
        500      | 0           | [110.4f ,300f, 0.0f]  || true  | true     | 1
        30       | 0           | [110.4f ,1000f, 0.0f] || true  | true     | 1
        155      | 4           | [0.0f ,0.0f, 0.0f]    || true  | true     | -1
        170      | 1           | [45.3f ,10.3f, 47.7f] || true  | true     | 3
    }

}

