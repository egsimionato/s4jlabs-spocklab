package s4j.ecom;

/**
 * Buying Proudction in a electronic shop:
 *  - Buyer
 *  - Products
 *  - Basket
 *  - Checkout
 *
 **/
public class Product {
    public String name;
    public Integer price;
    public Integer weight;
}

public class Basket {
    public Product product;
    public Integer currentWeight;


    public void addProduct(Product product) {
        this.product = product;
        this.currentWeight = product.weight;
    }

}

/* BasketWeightSpec */
class BasketWeightSpec extends spock.lang.Specification {

    def "A basket with one product has equal weight"() {
        given: "an empty basket and a TV"
        Product tv = new Product(name:"bravia", price:1200, weight: 18);
        Basket basket = new Basket();

        when: "user wants to buy the TV"
        basket.addProduct(tv);

        then: "basket weight is equal to the TV"
        basket.currentWeight == tv.weight;
    }
}
