package s4j.creditcard;


class Client {
    private boolean bonus;

    /* Getters and Setters */
    public void setBonus(boolean bonus) {
        this.bonus = bonus;
    }
    public boolean hasBonus() {
        return bonus;
    }

    /* behaviours */
    public boolean rejectsCharge() {
        bonus = false;
    }
}

class CreditCardBilling {

    public void charge(Client client, int amount) {
        client.setBonus(amount>100);
    }
}

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class CreditCardBillingTest {

    @Test
    public void shouldNotHaveBounsIfSpendsLessOf100() {
        Client client = new Client();
        CreditCardBilling billing = new CreditCardBilling();
        billing.charge(client, 90);
        assertFalse("expect no bonus", client.hasBonus());
    }

    @Test
    public void shouldHaveBonusIfSpendMoreOf100() {
        Client client = new Client();
        CreditCardBilling billing = new CreditCardBilling();
        billing.charge(client, 150);
        assertTrue("expect bonus", client.hasBonus());
    }

    @Test
    public void shouldLosesBonesIfNotAcceptTheTransaction() {
        Client client = new Client();
        CreditCardBilling billing = new CreditCardBilling();
        billing.charge(client, 150);
        client.rejectsCharge();
        assertFalse("expect no bonus", client.hasBonus());
    }

}

class CreditCardBillingSpec extends spock.lang.Specification {

    def "Client should not have a bonus if he spends less than 100 dollars" () {
        given: "that a client with a credit card billing"
        def client = new Client();
        def billing = new CreditCardBilling();

        when: "a client buys something with value at less of 100"
        billing.charge(client, 90);

        then: "the client should have the bonus option inactive"
        !client.hasBonus()
    }

    def "Client should have a bonus if he spends more than 100 dollars" () {
        given: "that a client with a credit card billing"
        def client = new Client();
        def billing = new CreditCardBilling();

        when: "a client buys something with value at least 100"
        billing.charge(client, 150);

        then: "the client should have the bonus option active"
        client.hasBonus()
    }

    def "Client loses bones is he does not accept the transaction" () {
        given: "that a client with a credit card billing"
        def client = new Client();
        def billing = new CreditCardBilling();

        when: "a client buys something and later changes mind"
        billing.charge(client, 150);
        client.rejectsCharge();

        then: "client should have the bonus option inactive"
        !client.hasBonus();

    }

}
