package s4j.fire;

import spock.lang.*;

class WarningStatus {
    private final boolean alarmActive;
    private final boolean fireDepartmentNotified;
    private final int activeSensors;
    private final int totalSensors;

    public WarningStatus( boolean alarmActive, boolean fireDepartmentNotified,
                          int activeSensors, int totalSensors ) {

        this.alarmActive = alarmActive;
        this.fireDepartmentNotified = fireDepartmentNotified;
        this.activeSensors = activeSensors;
        this.totalSensors = totalSensors;
    }

    public boolean isAlarmActive() { return alarmActive; }
    public boolean isFireDepartmentNotified() { return fireDepartmentNotified; }
    public int getActiveSensors() { return activeSensors; }
    public int getTotalSensors() { return totalSensors; }

}

class FireMonitor { /* FireEarlyWarning */
    private int sensorsDetectingFire = 0;
    private int totalSensors = 0;

    public void feedData(int triggeredFireSensors, int allSensors) {
        sensorsDetectingFire = triggeredFireSensors;
        totalSensors = allSensors;
    }

    public WarningStatus getCurrentStatus() {

        return new WarningStatus(
                sensorsDetectingFire > 0,
                sensorsDetectingFire > (totalSensors / 2.0),
                sensorsDetectingFire,
                totalSensors
            );
    }
}

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

public class FireSensorTest {

    @Test
    public void everythingIsOk() {
        FireMonitor fireEarlyWarning = new FireMonitor();
        int triggeredSensors = 0;
        int totalSensors = 10;

        fireEarlyWarning.feedData(triggeredSensors, totalSensors);
        WarningStatus status = fireEarlyWarning.getCurrentStatus();

        assertFalse("Expected no alarm", status.isAlarmActive());
        assertFalse("No notifications", status.isFireDepartmentNotified());
    }

    @Test
    public void oneSensorIsTriggered() {
        FireMonitor fireEarlyWarning = new FireMonitor();
        int triggeredSensors = 1;
        int totalSensors = 10;

        fireEarlyWarning.feedData(triggeredSensors, totalSensors);
        WarningStatus status = fireEarlyWarning.getCurrentStatus();

        assertTrue("Alarm sounds", status.isAlarmActive());
        assertFalse("No notifications", status.isFireDepartmentNotified());
        assertEquals("Notify if more than half sensors are active",
            status.isFireDepartmentNotified(),
            (status.getActiveSensors() > status.getTotalSensors() / 2.0));
    }

    @Test
    public void sevenSensorsAreTriggered() {
        FireMonitor fireEarlyWarning = new FireMonitor();
        int triggeredSensors = 7;
        int totalSensors = 10;

        fireEarlyWarning.feedData(triggeredSensors, totalSensors);
        WarningStatus status = fireEarlyWarning.getCurrentStatus();

        assertTrue("Alarm sounds", status.isAlarmActive());
        assertTrue("Fire Department is notified",
            status.isFireDepartmentNotified());
        assertEquals("Notify if more than half sensors are active",
            status.isFireDepartmentNotified(),
            (status.getActiveSensors() > status.getTotalSensors() / 2.0));
    }

}

class FireMonitorSystemSpec extends spock.lang.Specification {

    def "If all sensor are inactive everything is ok" () {
        given: "that all fire sensor are off"
        FireMonitor fireMonitor = new FireMonitor();
        int triggeredSensors = 0;
        int totalSensors = 10;

        when: "we ask the status of fire control"
        fireMonitor.feedData(triggeredSensors, totalSensors);
        WarningStatus status = fireMonitor.getCurrentStatus();

        then: "no alarm/notification should be triggered"
        !status.alarmActive;
        !status.fireDepartmentNotified;
        !(status.activeSensors > (status.totalSensors / 2.0))
        status.activeSensors == 0
        status.totalSensors == 10

    }

    def "If less than half of sensors are active the alarm should sound as a precuation" () {
        given: "that three fire sensors are active"
        FireMonitor fireMonitor = new FireMonitor();
        int triggeredSensors = 3;
        int totalSensors = 10;

        when: "we ask the status of fire control"
        fireMonitor.feedData(triggeredSensors, totalSensors);
        WarningStatus status = fireMonitor.getCurrentStatus();

        then: "only the alarm should be triggered"
        status.alarmActive;
        !status.fireDepartmentNotified;
        !(status.activeSensors > (status.totalSensors / 2.0))
        status.activeSensors == 3
        status.totalSensors == 10

    }

    def "If more than half of sensors are active then we have a fire" () {
        given: "that seven fire sensors are active"
        FireMonitor fireMonitor = new FireMonitor();
        int triggeredSensors = 7;
        int totalSensors = 10;

        when: "we ask the status of fire control"
        fireMonitor.feedData(triggeredSensors, totalSensors);
        WarningStatus status = fireMonitor.getCurrentStatus();

        then: "alarm is triggered and the fire department is notified"
        status.alarmActive;
        status.fireDepartmentNotified
        status.activeSensors > (status.totalSensors / 2.0)
        status.activeSensors == 7
        status.totalSensors == 10
    }

}

