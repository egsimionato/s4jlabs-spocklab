package s4j.temperature;

public interface TemperatureReader {
    public TemperatureReadings getCurrentReadings();
}

public class TemperatureReadings {
    public final List<Long> data;
    public TemperatureReadings(List<Long> temperatures) {
        this.data = temperatures;
    }
}

public class TemperatureMonitor {
    private final TemperatureReader reader;
    private TemperatureReadings lastReadings;
    private TemperatureReadings currentReadings;

    public TemperatureMonitor(TemperatureReader reader) {
        this.reader = reader;
    }

    public void readSensor() {
        lastReadings = currentReadings;
        currentReadings = reader.getCurrentReadings();
    }

    public boolean isTemperatureNormal() {
        boolean isNormal = false;
        currentReadings.data.eachWithIndex { currentMeter, i ->
            println "current[$i]:last[$i] => [${currentMeter}:${lastReadings.data[i]}] => ${Math.abs(currentMeter - lastReadings.data[i])}"
            if (Math.abs(currentMeter - lastReadings.data[i]) > 20) {
                isNormal = true;
            }
        }
        return isNormal;
    }
}

class TemperatureMonitorSpec extends spock.lang.Specification {

    def "If current temperature difference is within limits everything is ok"() {
        given: "that temperature readings are within limits"
        TemperatureReadings prev = new TemperatureReadings([20.0f, 40.0f, 80.0f]);
        TemperatureReadings current = new TemperatureReadings([30.0f, 45.0f, 73.0f]);
        TemperatureReader reader = Stub(TemperatureReader);
        reader.getCurrentReadings() >>> [prev, current];
        TemperatureMonitor monitor = new TemperatureMonitor(reader);

        when: "we ask the status of temperature control"
        monitor.readSensor();
        monitor.readSensor();

        then: "everything should be ok"
        !monitor.isTemperatureNormal();
    }

    def "If current temperature difference is more than 20 degrees the alarm should sound"() {
        given: "that temperature readings are not within limits"
        TemperatureReadings prev = new TemperatureReadings([20.0f, 40.0f, 80.0f]);
        TemperatureReadings current = new TemperatureReadings([30.0f, 10.0f, 73.0f]);
        TemperatureReader reader = Stub(TemperatureReader);
        reader.getCurrentReadings() >>> [prev, current];
        TemperatureMonitor monitor = new TemperatureMonitor(reader);

        when: "we ask the status of temperature control"
        monitor.readSensor();
        monitor.readSensor();

        then: "the alarm should sound"
        monitor.isTemperatureNormal();
    }
}
