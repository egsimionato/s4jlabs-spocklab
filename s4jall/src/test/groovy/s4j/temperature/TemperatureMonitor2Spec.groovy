package s4j.temperature2;

public class TemperatureReadings {
    public final List<Long> data;
    public TemperatureReadings(List<Long> data) {
        this.data = data;
    }
}

public interface TemperatureReader {
    public TemperatureReadings getCurrentReadings();
}

public interface IReactorControl {

    public void shutdownReactor();
    public void activateAlarm();

}

public class ReactorControl implements IReactorControl {
    public void shutdownReactor() {
        System.out.println("> Reactor shutting down");
    }
    public void activateAlarm() {
        System.out.println("> Reactor alarm activates");
    }
}

public class ImprovedTemperatureMonitor {
    private final TemperatureReader reader;
    private TemperatureReadings lastReadings;
    private TemperatureReadings currentReadings;
    private final IReactorControl control;

    public ImprovedTemperatureMonitor(TemperatureReader reader, IReactorControl control) {
        this.reader = reader;
        this.control = control;
    }
    public void readSensor() {
        lastReadings = currentReadings;
        currentReadings = reader.getCurrentReadings();
        if (lastReadings == null) return null;
        if (isTemperatureDiffMoreThan(20)) control.activateAlarm();
        if (isTemperatureDiffMoreThan(50)) control.shutdownReactor();
    }
    public boolean isTemperatureNormal() {
        return isTemperatureDiffMoreThan(20.0f);
    }
    public boolean isTemperatureCritical() {
        return isTemperatureDiffMoreThan(50.0f);
    }
    public boolean isTemperatureDiffMoreThan(long limit) {
        boolean isMore = false;
        currentReadings.data.eachWithIndex { currentMeter, i ->
            if (Math.abs(currentMeter - lastReadings.data[i]) > limit) {
                isMore = true;
            }
        }
        return isMore;

    }
}

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TemperatureMonitoryTest { /* CoolantSensorTest */

    @Test
    public void temperatureWithinLimits() {
        TemperatureReadings prev =
            new TemperatureReadings([20.0f, 40.0f, 80.0f]);
        TemperatureReadings current =
            new TemperatureReadings([30.0f, 45.0f, 73.0f]);

        TemperatureReader reader = mock(TemperatureReader.class);
        when(reader.getCurrentReadings()).thenReturn(prev, current);

        IReactorControl control = mock(IReactorControl.class);

        ImprovedTemperatureMonitor monitor =
            new ImprovedTemperatureMonitor(reader, control);

        monitor.readSensor();
        monitor.readSensor();

        verify(control, times(0)).activateAlarm();
        verify(control, times(0)).shutdownReactor();
    }

    @Test
    public void temperatureOutsideOfLimits() {
        TemperatureReadings prev =
            new TemperatureReadings([20.0f, 40.0f, 80.0f]);
        TemperatureReadings current =
            new TemperatureReadings([30.0f, 10.0f, 73.0f]);

        TemperatureReader reader = mock(TemperatureReader.class);
        when(reader.getCurrentReadings()).thenReturn(prev, current);

        IReactorControl control = mock(IReactorControl.class);

        ImprovedTemperatureMonitor monitor =
            new ImprovedTemperatureMonitor(reader, control);

        monitor.readSensor();
        monitor.readSensor();

        verify(control, times(1)).activateAlarm();
        verify(control, times(0)).shutdownReactor();
    }

    @Test
    public void extremeTemperatureChanges() {
        TemperatureReadings prev =
            new TemperatureReadings([20.0f, 40.0f, 80.0f]);
        TemperatureReadings current =
            new TemperatureReadings([30.0f, 10.0f, 160.0f]);

        TemperatureReader reader = mock(TemperatureReader.class);
        when(reader.getCurrentReadings()).thenReturn(prev, current);

        IReactorControl control = mock(IReactorControl.class);

        ImprovedTemperatureMonitor monitor =
            new ImprovedTemperatureMonitor(reader, control);

        monitor.readSensor();
        monitor.readSensor();

        verify(control, times(1)).activateAlarm();
        verify(control, times(1)).shutdownReactor();
    }
}

class TemperatureMonitor2Spec extends spock.lang.Specification {

    def "If current temperature difference is within limits everything is ok"() {
        given: "that temperature readings are within limits"
        TemperatureReadings prev = new TemperatureReadings([0.0f, 0.0f, 0.0f]);
        TemperatureReadings current = new TemperatureReadings([0.0f, 0.0f, 0.0f]);
        TemperatureReader reader = Stub(TemperatureReader);
        reader.getCurrentReadings() >>> [prev, current];
        ReactorControl control = Mock(ReactorControl);
        ImprovedTemperatureMonitor monitor = new ImprovedTemperatureMonitor(reader, control);

        when: "we ask the status of temperature control"
        monitor.readSensor();
        monitor.readSensor();

        then: "everything should be ok"
        0 * control.activateAlarm();
        0 * control.shutdownReactor();
    }

    def "If current temperature difference is more than 20 degrees the alarm sounds"() {
        given: "that temperature readings are not within limits"
        TemperatureReadings prev = new TemperatureReadings([20.0f, 40.0f, 80.0f]);
        TemperatureReadings current = new TemperatureReadings([30.0f, 10.0f, 73.0f]);
        TemperatureReader reader = Stub(TemperatureReader);
        reader.getCurrentReadings() >>> [prev, current];
        ReactorControl control = Mock(ReactorControl);
        ImprovedTemperatureMonitor monitor = new ImprovedTemperatureMonitor(reader, control);

        when: "we ask the status of temperature control"
        monitor.readSensor();
        monitor.readSensor();

        then: "everything should be ok"
        1 * control.activateAlarm();
        0 * control.shutdownReactor();
    }


	def "If current temperature difference is more than 50 degrees the reactor shuts down"() {
        given: "that temperature readings are not within limits"
        TemperatureReadings prev = new TemperatureReadings([20.0f, 40.0f, 80.0f]);
        TemperatureReadings current = new TemperatureReadings([30.0f, 10.0f, 160.0f]);
        TemperatureReader reader = Stub(TemperatureReader);
        reader.getCurrentReadings() >>> [prev, current];
        ReactorControl control = Mock(ReactorControl);
        ImprovedTemperatureMonitor monitor = new ImprovedTemperatureMonitor(reader, control);

        when: "we ask the status of temperature control"
        monitor.readSensor();
        monitor.readSensor();

        then: "the alarm should sound and the reactor should shut down"
        1 * control.activateAlarm();
        1 * control.shutdownReactor();
    }

    def "Testing of all 3 sensors with temperatures that rize and fall"() {
        given: "various temperature readings"
        TemperatureReadings prev = new TemperatureReadings(previousTemp);
        TemperatureReadings current = new TemperatureReadings(currentTemp);
        TemperatureReader reader = Stub(TemperatureReader)
        reader.getCurrentReadings() >>> [prev, current]
        ReactorControl control = Mock(ReactorControl)
        ImprovedTemperatureMonitor monitor = new ImprovedTemperatureMonitor(reader,control)

        when: "we ask the status of temperature control"
        monitor.readSensor()
        monitor.readSensor()

        then: "the alarm should sound and the reactor should shut down if needed"
        alarm * control.activateAlarm()
        shutDown * control.shutdownReactor()

        where: "possible temperatures are:"
        previousTemp | currentTemp       ||  alarm | shutDown
        [20, 30, 40]| [25, 15, 43.2]     ||     0  | 0
        [20, 30, 40]| [13.3, 37.8, 39.2] ||     0  | 0
        [20, 30, 40]| [50, 15, 43.2]     ||     1  | 0
        [20, 30, 40]| [-20, 15, 43.2]    ||     1  | 0
        [20, 30, 40]| [100, 15, 43.2]    ||     1  | 1
        [20, 30, 40]| [-80, 15, 43.2]    ||     1  | 1
        [20, 30, 40]| [20, 55, 43.2]     ||     1  | 0
        [20, 30, 40]| [20, 8  , 43.2]    ||     1  | 0
        [20, 30, 40]| [21, 100, 43.2]    ||     1  | 1
        [20, 30, 40]| [22, -40, 43.2]    ||     1  | 1
        [20, 30, 40]| [20, 35, 76]       ||     1  | 0
        [20, 30, 40]| [20, 31  ,13.2]    ||     1  | 0
        [20, 30, 40]| [21, 33, 97]       ||     1  | 1
        [20, 30, 40]| [22, 39, -22]      ||     1  | 1
    }


}
