package s4j.calculator;

public class Multiplier {

    public int multiply(int a, int b) {
        return a * b;
    }

}

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MultiplierTest {

    @Test
    public void simpleMultiplicationTest() {
        Multiplier multi = new Multiplier();
        assertEquals("3 times 7 is 21",
                21, multi.multiply(3, 7));
    }

    @Test
    public void combinedOperationsTest() {
        Adder adder = new Adder();
        Multiplier multi = new Multiplier();

        assertEquals("4 times (2 plus 3) is 20",
                20, multi.multiply(4, adder.add(2, 3)));
        assertEquals("(2 plus 3) times 4 is also 20",
                20, multi.multiply(adder.add(2, 3), 4));
    }
}

class MultiplierSpec extends spock.lang.Specification {

    def "Multiply two numbers and return the result" () {
        given: "a new Multiplier class is created"
        def multi = new Multiplier();
        when: true
        then: "3 times 7 is 21"
        multi.multiply(3, 7) == 21
    }

    def "Combine bot multiplication and addition" () {
        given: "a new Multiplier and adder classes are created"
        def adder = new Adder();
        def multi = new Multiplier();
        when: true
        then: "4 times (2 plus 3) is 20"
        multi.multiply(4, adder.add(2, 3)) == 20
        and: "(2 plus 3) times 4 is also 20"
        multi.multiply(adder.add(2, 3), 4) == 20
    }

}

