package s4j.output.ansi;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

public class PrintColorWriter extends PrintWriter {

    public PrintColorWriter(PrintStream out)
        throws UnsupportedEncodingException {
        super(new OutputStreamWriter(out, "UTF-8"), true);
    }

    public void println(AnsiColor color, String string) {
        print(color.getCode());
        print(string);
        println(AnsiColor.RESET);
        flush();
    }

    public void green(String string) {
        println(AnsiColor.GREEN, string);
    }

    public void red(String string) {
        println(AnsiColor.RED, string);
    }

}
