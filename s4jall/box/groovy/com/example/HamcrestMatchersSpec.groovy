import static spock.util.matcher.HamcrestMatchers.closeTo

class HamcrestMatchersSpec extends spock.lang.Specification {

    def "comparing two decimal numbers" () {
        def myPi = 3.14

        expect:
            myPi closeTo(Math.PI, 0.01)
    }
}
