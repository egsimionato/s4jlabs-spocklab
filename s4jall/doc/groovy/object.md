
Groovy treats all objects as true unless
========================================
* The object is an empty string.
* The object is a null reference.
* The object is the zero number.
* The object is an empty collection (map, list, array, and so on).
* The object is the false Boolean (obviously).
* The object is a regex matcher that fails.
